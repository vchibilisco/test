## Pasos

# Configuracion de postgres

1 - Ejecutar `docker-compose build`
2 - Ejecutar `docker-compose up`
3 - Crear una base de datos llamada `test`

# Instalación

1 - Ejecutar `npm install`

# Ejecutar aplicaciones

1 - Backend: `nx serve api`
1 - Frontend: `nx serve web`

