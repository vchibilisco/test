import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthService,
    private router: Router
  ){}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = this.authenticationService.authSubject.value;
    if (localStorage.getItem("ACCESS_TOKEN")) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem("ACCESS_TOKEN")}`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        console.log(error);
        if (error && error.status === 401) {
          this.authenticationService.reset();
          this.router.navigateByUrl("auth");
        } else {
          return throwError(error);
        }
      })
    );
  }
}