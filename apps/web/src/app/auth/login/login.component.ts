import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password:['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  onSubmit() {
    if (this.user.valid) {
      this.authService.login(this.user.value).subscribe(
        () => {
          console.log("Logged in!");
          this.authService.ifLoggedIn();
          // this.router.navigateByUrl('users');
        },
        // () => {},
        error => console.log(error)
      );
    }
  }

  goToRegister() {
    this.router.navigateByUrl('register');
  }
}
