import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password:['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  onSubmit() {
    if (this.user.valid) {
      this.authService.register(this.user.value).subscribe(
        data => this.router.navigateByUrl('auth'),
        error => console.log(error),
      );
    }
  }

  goToLogin() {
    this.router.navigateByUrl('auth');
  }
}
