import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthResponse } from './auth-response';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authSubject = new BehaviorSubject(false);

  constructor(
    private http: HttpClient,
    private route: Router
  ) {
    this.ifLoggedIn();
  }

  ifLoggedIn() {
    const token = localStorage.getItem("ACCESS_TOKEN");
    console.log("token => ", token);
    if (token) {
      this.authSubject.next(true);
      console.log("mostramela! ", this.authSubject.value);
    }
  }

  login(user): Observable<AuthResponse> {
    return this.http.post('/api/auth/login', user).pipe(
      tap(async (res: AuthResponse) => {
        if (res.email) {
          localStorage.setItem("ACCESS_TOKEN", res.access_token);
          // this.authSubject.next(true);
          // console.log("mostramela! ", this.authSubject.value);
        }
      })
    );
  }

  register(user) {
    return this.http.post('/api/auth/register', user);
  }

  isAuthenticated() {
    // return Boolean(this.authSubject.value || localStorage.getItem("ACCESS_TOKEN"));
    return this.authSubject.value;
  }

  reset() {
    localStorage.removeItem("ACCESS_TOKEN");
    this.authSubject.next(false);
  }
}
