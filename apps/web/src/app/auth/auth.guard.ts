import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService
  ){}

  canActivate(): boolean {
    const auth = this.authService.isAuthenticated();
    console.log("canActive => ", auth);
    return this.authService.isAuthenticated();
  }
}
