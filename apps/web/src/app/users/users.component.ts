import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'login-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: Object[] = [];

  constructor(
    private usersService: UsersService
  ) {
    this.usersService.findAll().subscribe(
      (data: Object[]) => this.users = data,
      error => console.log(error)
    );
  }

  ngOnInit(): void {
  }

}
