import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isLogged: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.authService.authSubject.subscribe(
      state => {
        console.log("valor despues del log in => ",state);
        if(state) {
          this.isLogged = true;
          this.router.navigateByUrl("users");
        }
        else {
          this.isLogged = false;
          this.router.navigateByUrl("auth");
        }
      }
    );
  }
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  logout() {
    this.authService.reset();
  }
}
