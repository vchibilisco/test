import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';
import { UserEntity } from '../entities/UserEntity';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private repository: Repository<UserEntity>,
    private jwtService: JwtService
  ){}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.repository.findOne({
      where: {
        email
      }
    });

    if (user && await compareSync(pass, user.password)) {
      const {
        id,
        email
      } = user;

      return {
        id,
        email
      };
    }
    return null;
  }

  async login(user: UserEntity) {
    const userValid = await this.validateUser(user.email, user.password);
    if (!userValid) {
      return null;
    }

    const payload = {
      email: userValid.email,
      sub: userValid.id
    };

    return {
      email: userValid.email,
      access_token: this.jwtService.sign(payload),
    };
  }

  async register(user: UserEntity): Promise<UserEntity> {
    const userExist = await this.repository.findOne({
      email: user.email
    });
    console.log(user);
    console.log(userExist);
    if (userExist) {
      return null;
    }
    return await this.repository.save(user);
  }
}
