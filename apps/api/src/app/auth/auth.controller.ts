import { Controller, Post, Body, HttpException, HttpStatus } from '@nestjs/common';
import { UserLoginDTO } from '../dto/UserDto';
import { UserEntity } from '../entities/UserEntity';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService
  ){}

  @Post('login')
  async login(@Body() body: UserLoginDTO) {
    const user = new UserEntity();
    user.email = body.email;
    user.password = body.password;
    const response = await this.authService.login(user);
    if (!response) {
      throw new HttpException({
        status: HttpStatus.UNAUTHORIZED,
        error: "Email and/or password are invalid."
      }, HttpStatus.UNAUTHORIZED);
    }
    return response;
  }

  @Post('register')
  async register(@Body() body: UserLoginDTO) {
    const user = new UserEntity();
    user.email = body.email;
    user.password = body.password;

    const response = await this.authService.register(user);

    if (!response) {
      throw new HttpException({
        status: HttpStatus.PRECONDITION_FAILED,
        error: "User exist."
      }, HttpStatus.PRECONDITION_FAILED);
    }

    return response;
  }
}
