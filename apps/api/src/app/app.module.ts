import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { UserEntity } from './entities/UserEntity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.NX_DB_HOST,
      port: Number(process.env.NX_DB_PORT),
      username: process.env.NX_DB_USERNAME,
      password: process.env.NX_DB_PASSWORD,
      database: process.env.NX_DB_DATABASE,
      entities: [UserEntity],
      synchronize: true,
      autoLoadEntities: true
    }),
    AuthModule,
    UsersModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
