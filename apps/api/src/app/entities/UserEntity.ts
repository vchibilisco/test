import { PrimaryGeneratedColumn, Column, BeforeInsert, CreateDateColumn, UpdateDateColumn, BeforeUpdate, Entity } from 'typeorm';
import { genSaltSync, hashSync } from 'bcrypt';

const saltRounds = 10;

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  email: string;
  @Column()
  password: string;
  @CreateDateColumn({ type: 'timestamp', name: 'created_at', nullable: false })
  createdAt: Date;
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at', nullable: false })
  updatedAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  hashPassword() {
    if (this.password) {
      const salt = genSaltSync(saltRounds);
      this.password = hashSync(this.password, salt);
    }
  }
}